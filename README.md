# Flutter Cupertino Superellipse

[pub package](https://pub.dartlang.org/packages/cupertino_superellipse)

Formula-based superellipse implementation for Flutter. Compared to traditional BorderRadius it provides more optically balanced shape:

![demo screenshot](img/demo.png)

## Usage

#### 1\. Depend

Add this to you package's `pubspec.yaml` file:

```yaml
dependencies:
  cupertino_superellipse: ^1.0.1
```

#### 2\. Install

Run command:

```bash
$ flutter packages get
```

#### 3\. Import

Import in Dart code:

```dart
import 'package:cupertino_superellipse/cupertino_superellipse.dart';
```

#### 4\. Use

```dart
// n: superellipse n>2 property
// corners: square/rounded corners (BR, BL, TL, TR)
new Material(
  clipBehavior: Clip.antiAlias,
  shape: SuperEllipse(
    n: 4,
    corners: [true, true, true, true],
  ),
  color: Colors.white,
  child: new Container(
    height: 300,
    width: 300,
  ),
),
```