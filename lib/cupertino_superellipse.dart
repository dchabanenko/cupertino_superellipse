import 'dart:math';
import 'package:flutter/cupertino.dart';

class Point {
  double x;
  double y;

  Point(double x, double y) {
    this.x = x;
    this.y = y;
  }
}

class SuperEllipse extends ShapeBorder {
  final BorderSide side;
  final double n;
  final List corners;

  SuperEllipse({
    @required this.n,
    this.corners = const [true, true, true, true],
    this.side = BorderSide.none,
  }) : assert(side != null);

  @override
  EdgeInsetsGeometry get dimensions => EdgeInsets.all(side.width);

  @override
  ShapeBorder scale(double t) {
    return SuperEllipse(
      side: side.scale(t),
      n: n,
      corners: corners,
    );
  }

  @override
  Path getInnerPath(Rect rect, {TextDirection textDirection}) {
    return _superEllipsePath(rect, n, corners);
  }

  @override
  Path getOuterPath(Rect rect, {TextDirection textDirection}) {
    return _superEllipsePath(rect, n, corners);
  }

  static Path _superEllipsePath(Rect rect, double n, List corners) {
    bool wide = rect.width > rect.height;
    int centerX = rect.width ~/ 2;
    int centerY = rect.height ~/ 2;
    int a = centerX;
    int b = centerY;
    int delta = (a - b).abs();
    List<Point> points = new List();
    List<Point> points2 = new List();

    for (int i = min(a, b); i >= 0; i--) {
      double val =
          pow((i.toDouble() / min(a, b).toDouble()).abs(), n.toDouble());
      points.add(new Point(
          i.toDouble(), min(a, b).toDouble() * pow(1 - val, 1 / n.toDouble())));
    }

    for (int i = min(a, b); i >= 0; i--) {
      double val =
          pow((i.toDouble() / min(a, b).toDouble()).abs(), n.toDouble());
      points2.add(new Point(
          min(a, b).toDouble() * pow(1 - val, 1 / n.toDouble()), i.toDouble()));
    }

    int middle;
    int middle2;
    for (int j = 0; j < points.length; j++) {
      double val1 = num.parse(points[j].y.toStringAsFixed(12));
      double val2 =
          num.parse(points2[points2.length - 1 - j].x.toStringAsFixed(12));
      if (val1 == val2) {
        middle = j;
        middle2 = points2[j].x.toInt();
      }
    }

    Path path = new Path();
    path.moveTo(centerX + points2[middle2].x + (wide ? delta : 0),
        centerY - points2[middle2].y - (wide ? 0 : delta));

    if (corners[3]) {
      for (int i = middle2; i > 0; i--)
        path.lineTo(centerX + points2[i].x + (wide ? delta : 0),
            centerY - points2[i].y - (wide ? 0 : delta));
      for (int i = middle; i < points.length - 1; i++)
        path.lineTo(centerX + points[i].x + (wide ? delta : 0),
            centerY - points[i].y - (wide ? 0 : delta));
    } else
      path.lineTo(centerX.toDouble() + a, centerY.toDouble() - b);

    if (corners[2]) {
      for (int i = points.length - 1; i > middle; i--)
        path.lineTo(centerX - points[i].x - (wide ? delta : 0),
            centerY - points[i].y - (wide ? 0 : delta));
      for (int i = 1; i < middle2; i++)
        path.lineTo(centerX - points2[i].x - (wide ? delta : 0),
            centerY - points2[i].y - (wide ? 0 : delta));
    } else
      path.lineTo(centerX.toDouble() - a, centerY.toDouble() - b);

    if (corners[1]) {
      for (int i = middle2; i > 0; i--)
        path.lineTo(centerX - points2[i].x - (wide ? delta : 0),
            centerY + points2[i].y + (wide ? 0 : delta));
      for (int i = middle; i < points.length - 1; i++)
        path.lineTo(centerX - points[i].x - (wide ? delta : 0),
            centerY + points[i].y + (wide ? 0 : delta));
    } else
      path.lineTo(centerX.toDouble() - a, centerY.toDouble() + b);

    if (corners[0]) {
      for (int i = points.length - 1; i > middle; i--)
        path.lineTo(centerX + points[i].x + (wide ? delta : 0),
            centerY + points[i].y + (wide ? 0 : delta));
      for (int i = 1; i < middle2; i++)
        path.lineTo(centerX + points2[i].x + (wide ? delta : 0),
            centerY + points2[i].y + (wide ? 0 : delta));
    } else
      path.lineTo(centerX.toDouble() + a, centerY.toDouble() + b);

    path.lineTo(centerX + points2[middle2].x + (wide ? delta : 0),
        centerY - points2[middle2].y - (wide ? 0 : delta));
    return path;
  }

  @override
  void paint(Canvas canvas, Rect rect, {TextDirection textDirection}) {
    Path path = getOuterPath(rect, textDirection: textDirection);
    canvas.drawPath(path, side.toPaint());
  }
}
